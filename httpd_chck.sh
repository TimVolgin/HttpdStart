#!/bin/bash

if !(pgrep -x "httpd" > /dev/null); then	
  dt=$(date '+%d/%m/%Y %H:%M:%S');
  echo '<'$dt'> httpd was not running, starting...' >> /root/httpd_start.log;
  /root/homework/materials/class03/src/tinyhttpd/tinyhttpd/httpd
fi